FROM node
WORKDIR /root

# Copy API code and install required 3rd party modules.
ADD app/ .
RUN npm install

# Declare port and entrypoint.
EXPOSE 3000
ENTRYPOINT ["npm", "start"]