const utils = require("../../utils");
const request = require('request');

var querystring = require('querystring');


var AZURE_CLIENT_ID = utils.env("AZURE_CLIENT_ID", false);
var AZURE_CLIENT_SECRET = utils.env("AZURE_CLIENT_SECRET", false);
var AZURE_TENANT = utils.env("AZURE_TENANT", false);

var storedToken = {
    "token_type": "",
    "access_token" : "",
    "expires_in" : new Date()
}

const getNewToken = (callback) => {
    var params = {
        client_id: AZURE_CLIENT_ID,
        client_secret: AZURE_CLIENT_SECRET,
        grant_type: "client_credentials",
        scope: "https://graph.microsoft.com/.default"
    }
    var formData = querystring.stringify(params)
    var contentLength = formData.length;

    request(`https://login.microsoftonline.com/${AZURE_TENANT}/oauth2/v2.0/token`, {
        method: "POST",
        headers: {
            'Content-Length': contentLength,
            "Content-Type": "application/x-www-form-urlencoded"
        },
        body: formData
    }, (err, resp, body) => {
        storedToken = JSON.parse(body);
        storedToken["expires_stamp"] = new Date(new Date().setSeconds(new Date().getSeconds() + storedToken["expires_in"]));
        callback(storedToken);
    });
}

const getAuthToken = (callback) => {
    new Date(storedToken["expires_stamp"]) > new Date() ?
        callback(storedToken):
        getNewToken(callback);
}

const getUserGroups = (email, callback) => {
    getAuthToken((token) => {
        request(`https://graph.microsoft.com/v1.0/users/${email}/memberOf`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `${token['token_type']} ${token['access_token']}`
            }
        },(err, resp, body) => {
            var groups = JSON.parse(body)["value"];
            var groupNames = [];
            for (group of groups) {
                groupNames.push(group['displayName'])
            }
            callback(groupNames);
        });
    });
}

const configured = () => (AZURE_CLIENT_ID && AZURE_CLIENT_SECRET && AZURE_TENANT) ? true : false;
module.exports = {
    getMembership: (email, callback) => {
        configured() ? getUserGroups(email, callback) : callback([]);
    }
}