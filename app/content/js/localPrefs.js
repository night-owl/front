const browserStorage = window.localStorage;

const storePUT = (name, value) => {
    browserStorage.setItem(name, value);
}
const storeDELETE = (name) => {
    browserStorage.removeItem(name);
}
const storeGET = (name, defaultValue) => {
    return browserStorage.getItem(name) ? browserStorage.getItem(name) : defaultValue;
}

const applyTheme = () => {
    var theme = storeGET("theme", "light");
    $("html").get(0).style.setProperty("--background-primary", `var(--${theme}-primary)`);
    $("html").get(0).style.setProperty("--background-secondary", `var(--${theme}-secondary)`);
    $("html").get(0).style.setProperty("--hover-primary", `var(--${theme}-hover-primary)`);
    $("html").get(0).style.setProperty("--font-primary", `var(--${theme}-font-primary)`);
    $("html").get(0).style.setProperty("--font-highlight", `var(--${theme}-font-highlight)`);
    $("html").get(0).style.setProperty("--theme-subtle", `var(--${theme}-darker)`);
    $("html").get(0).style.setProperty("--font-reversed", `var(--${theme}-font-reversed)`);

    $("html").get(0).style.setProperty("--theme-primary", `var(--theme-color-${theme})`);
    $("html").get(0).style.setProperty("--theme-secondary", `var(--theme-color-${theme})`);
}
applyTheme();