const express = require('express')
var bodyParser = require('body-parser')
const request = require('request');
var cookieParser = require('cookie-parser');


const utils = require('./utils.js')
var useragent = require('express-useragent');


var API_ENDPOINT = utils.env("API_ENDPOINT", "localhost");

var API_ADMIN_USER = utils.env("API_ADMIN_USER", "admin");
var API_ADMIN_PASSWORD = utils.env("API_ADMIN_PASSWORD", "admin");

var RBAC_ENABLED = utils.env("RBAC_ENABLED", false);
var RBAC_USER_COOKIE = utils.env("RBAC_USER_COOKIE", "default");
var RBAC_PROVIDER = utils.env("RBAC_PROVIDER", "default");
var RBAC_DEV = utils.env("RBAC_DEV", false);
var RBAC_DEV_COOKIE = utils.env("RBAC_DEV_COOKIE", false);

var AUTH_HEADER = "Basic " + Buffer.from(API_ADMIN_USER + ":" + API_ADMIN_PASSWORD).toString('base64')

const app = express()
const port = 3000

var cachedUsers = {

}

app.use(useragent.express());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());

app.listen(port, () => console.log(`Running on port ${port}`))

app.get("/", function (req, res) {
    res.sendFile('content/frame.html', { root: __dirname });
});
app.get("/css/global.css", (req, res) => {
    if(req.useragent.isMobile) {
        res.sendFile('/content/css/mobile.css', { root: __dirname });
    }else {
        res.sendFile('/content/css/global.css', { root: __dirname });
    }
});
app.use(express.static('content'));

const rbacAzure = require(`./app_modules/rbac_providers/${RBAC_PROVIDER}`);
const getMembership = (mail, callback) => {
    switch (RBAC_PROVIDER) {
        case "azure":
            rbacAzure.getMembership(mail, (membership) => {
                cachedUsers[mail] = membership;
                callback(membership)
            });
            break;
        default:
            cachedUsers[mail] = [
                "user"
            ];
            callback([
                "user"
            ]);
            break;
    }
}

const mailRegex = /([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+)/gm;
const cookieUser = (req, res, callback) => {
    var decode = new Buffer(req.cookies[RBAC_USER_COOKIE].split("|")[0], 'base64')
    var mail = decode.toString('ascii').match(mailRegex)[0];
    getMembership(mail, (membership) => {
        callback(res, { "username": mail, "rbac": true, "roles": [], "memberOf": membership});
    })
    // try {
    //     console.log(req.cookies);
    //     var decode = new Buffer(req.cookies[RBAC_USER_COOKIE].split("|")[0], 'base64')
    //     var mail = decode.toString('ascii').match(mailRegex)[0];
    //     getMembership(mail, (membership) => {
    //         callback(res, { "username": mail, "rbac": true, "roles": [], "memberOf": membership});
    //     })
    // } catch (err) {
    //     callback(res, { "username": null, "rbac": true, "roles": [] });
    // }
}
const buildDevCookie = () => {
    var req = {
        cookies : {}
    }
    req.cookies[RBAC_USER_COOKIE] = RBAC_DEV_COOKIE;
    return req;
}
const requestUser = (req, res) => {
    return RBAC_DEV ? RBAC_DEV_COOKIE ? 
    cookieUser(buildDevCookie(), res, sendRBACPayload) : 
    sendRBACPayload(res, { "rbac": false, "username": "dev@nightowl", "roles": ["admin"] }) : 
    cookieUser(req, res, sendRBACPayload);
};
const sendRBACPayload = (response, payload) => {
    response.send(payload);
}
app.get("/rbac/user", (req, res) => {
    var rbacData = RBAC_ENABLED ? 
        requestUser(req, res) : 
        sendRBACPayload(res, { "rbac": false, "username": "admin@nightowl", "roles": ["admin"] });    
});

app.get("/sponsor", function (req, res) {
    res.sendFile('content/sponsor.html', { root: __dirname });
});

var cache = {

}
var cache_urls = []

app.get('/healthz', (req, res) => {
    res.status(200).send("ok");
});

app.get("/cluster/clients", function (req, res) {
    request(API_ENDPOINT + '/api/cluster/clients', {
        json: true, headers: {
            "Authorization": AUTH_HEADER,
            "Content-Type": "application/json"
        }
    }, (err, resp, body) => {
        if (err) {
            res.send(err);
        } else {
            res.send(body);
        }
    });
});

app.get("/radius/users", function (req, res) {
    request(API_ENDPOINT + '/radius/users', {
        json: true, headers: {
            "Authorization": AUTH_HEADER,
            "Content-Type": "application/json"
        }
    }, (err, resp, body) => {
        if (err) {
            res.send(err);
        } else {
            res.send(body);
        }
    });
});

app.delete("/radius/users/*", function (req, res) {
    var name = (req.path.split('/'))[3];
    console.log(name);
    request(API_ENDPOINT + '/radius/users/' + name, {
        method: "DELETE", json: true, headers: {
            "Authorization": AUTH_HEADER,
            "Content-Type": "application/json"
        }
    }, (err, resp, body) => {
        if (err) {
            res.send(err);
        } else {
            res.send(body);
        }
    });
});

app.post("/radius/users/*", function (req, res) {
    var name = (req.path.split('/'))[3];
    console.log(name);
    console.log(req.body);
    request(API_ENDPOINT + '/radius/users/' + name, {
        method: "POST", json: true, headers: {
            "Authorization": AUTH_HEADER,
            "Content-Type": "application/json"
        },
        body: req.body
    }, (err, resp, body) => {
        if (err) {
            res.send(err);
        } else {
            res.send(body);
        }
    });
});

app.delete("/radius/clients/*", function (req, res) {
    var name = (req.path.split('/'))[3];
    console.log(name);
    request(API_ENDPOINT + '/radius/clients/' + name, {
        method: "DELETE", json: true, headers: {
            "Authorization": AUTH_HEADER,
            "Content-Type": "application/json"
        }
    }, (err, resp, body) => {
        if (err) {
            res.send(err);
        } else {
            res.send(body);
        }
    });
});

app.post("/radius/clients/*", function (req, res) {
    var name = (req.path.split('/'))[3];
    console.log(name);
    console.log(req.body);
    request(API_ENDPOINT + '/radius/clients/' + name, {
        method: "POST", json: true, headers: {
            "Authorization": AUTH_HEADER,
            "Content-Type": "application/json"
        },
        body: req.body
    }, (err, resp, body) => {
        if (err) {
            res.send(err);
        } else {
            res.send(body);
        }
    });
});

app.get("/radius/clients", function (req, res) {
    request(API_ENDPOINT + '/radius/clients', {
        json: true, headers: {
            "Authorization": AUTH_HEADER,
            "Content-Type": "application/json"
        }
    }, (err, resp, body) => {
        if (err) {
            res.send(err);
        } else {
            res.send(body);
        }
    });
})

app.get("/radius/status", function (req, res) {
    request(API_ENDPOINT + '/radius/status', {
        json: true, headers: {
            "Authorization": AUTH_HEADER,
            "Content-Type": "application/json"
        }
    }, (err, resp, body) => {
        if (err) {
            res.send(err);
        } else {
            res.send(body);
        }
    });
})

app.get("/tacacs/show", function (req, res) {
    request(API_ENDPOINT + '/api/tacacs/show', {
        json: true, headers: {
            "Authorization": AUTH_HEADER,
            "Content-Type": "application/json"
        }
    }, (err, resp, body) => {
        if (err) {
            res.send(err);
        } else {
            res.send(body);
        }
    });
})

app.get("/tacacs/logs", function (req, res) {
    request(API_ENDPOINT + '/api/tacacs/logs', {
        json: true, headers: {
            "Authorization": AUTH_HEADER,
            "Content-Type": "application/json",
            "total" : req.header('Total') || 50,
            "range" : req.header('Range') || 15
        }
    }, (err, resp, body) => {
        if (err) {
            res.send(err);
        } else {
            res.send(body);
        }
    });
})

app.get("/devices/*/scan", function (req, res) {
    var name = (req.path.split('/'))[2];
    console.log("sending scan for " + name);
    request(API_ENDPOINT + '/api/devices/' + name + "/scan", {
        json: true, headers: {
            "Authorization": AUTH_HEADER,
            "Content-Type": "application/json"
        }
    }, (err, resp, body) => {
        if (err) {
            res.send(err);
        } else {
            res.send(body);
        }
    });
})

app.get("/settings/*", function (req, res) {
    var name = (req.path.split('/'))[2];
    request(API_ENDPOINT + '/api/settings/' + name, {
        json: true, headers: {
            "Authorization": AUTH_HEADER,
            "Content-Type": "application/json"
        }
    }, (err, resp, body) => {
        if (err) {
            res.send(err);
        } else {
            res.send(body);
        }
    });
});

app.get("/credentials/*", function (req, res) {
    var name = (req.path.split('/'))[2];
    request(API_ENDPOINT + '/api/credentials/' + name, {
        json: true, headers: {
            "Authorization": AUTH_HEADER,
            "Content-Type": "application/json"
        }
    }, (err, resp, body) => {
        if (err) {
            res.send(err);
        } else {
            res.send(body);
        }
    });
});

app.get("/devices/*", function (req, res) {
    var name = (req.path.split('/'))[2];
    request(API_ENDPOINT + '/api/devices/' + name, {
        json: true, headers: {
            "Authorization": AUTH_HEADER,
            "Content-Type": "application/json"
        }
    }, (err, resp, body) => {
        if (err) {
            res.send(err);
        } else {
            res.send(body);
        }
    });
});

app.delete("/devices/*", function (req, res) {
    var name = (req.path.split('/'))[2];
    request(API_ENDPOINT + '/api/devices/' + name, {
        method: "DELETE", json: true, headers: {
            "Authorization": AUTH_HEADER,
            "Content-Type": "application/json"
        }
    }, (err, resp, body) => {
        if (err) {
            res.send(err);
        } else {
            res.send(body);
        }
    });
});

app.post("/devices/*", function (req, res) {
    var name = (req.path.split('/'))[2];
    console.log(name);
    console.log(req.body);
    request(API_ENDPOINT + '/api/devices/' + name, {
        method: "POST", json: true, headers: {
            "Authorization": AUTH_HEADER,
            "Content-Type": "application/json"
        },
        body: req.body
    }, (err, resp, body) => {
        if (err) {
            res.send(err);
        } else {
            res.send(body);
        }
    });
});

app.get("/devices", function (req, res) {
    request(API_ENDPOINT + '/api/devices', {
        json: true, headers: {
            "Authorization": AUTH_HEADER,
            "Content-Type": "application/json"
        }
    }, (err, resp, body) => {
        if (err) {
            res.send(err);
        } else {
            cache['devices'] = body;
            cache_urls.push(API_ENDPOINT + "/api/devices");
            res.send(body);
        }
    });
})

app.get("/api/tacas/status", function (req, res) {
    request(API_ENDPOINT + '/api/tacacs/status', {
        json: true, headers: {
            "Authorization": AUTH_HEADER,
            "Content-Type": "application/json"
        }
    }, (err, resp, body) => {
        if (err) {
            res.send(err);
        } else {
            res.send(body);
        }
    });
});

app.get("/api/tacas/restart", function (req, res) {
    request(API_ENDPOINT + '/api/tacacs/restart', {
        json: true, headers: {
            "Authorization": AUTH_HEADER,
            "Content-Type": "application/json"
        }
    }, (err, resp, body) => {
        if (err) {
            res.send(err);
        } else {
            res.send(body);
        }
    });
});

app.get("/api/tacas/configure", function (req, res) {
    request(API_ENDPOINT + '/api/tacacs/configure', {
        json: true, headers: {
            "Authorization": AUTH_HEADER,
            "Content-Type": "application/json"
        }
    }, (err, resp, body) => {
        if (err) {
            res.send(err);
        } else {
            res.send(body);
        }
    });
});